/* AtCore
    Copyright (C) <2016>

    Authors:
        Tomaz Canabrava <tcanabrava@kde.org>
        Patrick José Pereira <patrickjp@kde.org>
        Chris Rizzitello <rizzitello@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) version 3, or any
    later version accepted by the membership of KDE e.V. (or its
    successor approved by the membership of KDE e.V.), which shall
    act as a proxy defined in Section 6 of version 3 of the license.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library.  If not, see
   <http://www.gnu.org/licenses/>.
*/
#include "clientstuff.h"
#include <QLoggingCategory>
#include <QSslKey>
#include <QSslCertificate>
#include <QDataStream>

Q_LOGGING_CATEGORY(ATCORE_CLIENT, "org.kde.atelier.core.client")

ClientStuff::ClientStuff(QObject *parent)
    : QObject(parent)

{
    m_socket = new QSslSocket(this);

    connect(m_socket, &QSslSocket::disconnected, this, &ClientStuff::serverDisconnect);

    connect(m_socket , &QSslSocket::encryptedBytesWritten, this, &ClientStuff::encryptedBytesWritten);
    connect(m_socket, QOverload<const QList<QSslError> &>::of(&QSslSocket::sslErrors), this, &ClientStuff::sslErrors);
    m_socket->addCaCertificates(QStringLiteral("/../../../server/server.crt"));
    m_socket->setPrivateKey(QStringLiteral("/../../../client/client.key"));
    m_socket->setLocalCertificate(QStringLiteral("/../../../client/client.crt"));
    m_socket->setPeerVerifyMode(QSslSocket::VerifyPeer);
    m_socket->setProtocol(QSsl::TlsV1SslV3);
}

void ClientStuff::connectToHost() 
{
    m_socket->connectToHostEncrypted(tr("127.0.0.1"), 38917);
    if (m_socket->waitForEncrypted(5000)) {
       qCDebug(ATCORE_CLIENT) << (tr(" connected to server"));
       status = true;
    } else {
       qCDebug(ATCORE_CLIENT) << (tr("Unable to connect to server"));
       status = false;
       exit(0);
    }
}

void ClientStuff::encrypted() 
{
    qCDebug(ATCORE_CLIENT) << tr("Encrypted") << m_socket;
    if (!m_socket)
        return;
}

bool ClientStuff::getStatus()
{
    return status;
}

void ClientStuff::encryptedBytesWritten(qint64 written) 
{
    qCDebug(ATCORE_CLIENT) << tr("encryptedBytesWritten") << m_socket << written;
}

void ClientStuff::sslErrors(const QList<QSslError> &errors) 
{
    foreach (const QSslError &error, errors) {
            qCDebug(ATCORE_CLIENT) << error.errorString();
    }
}

void ClientStuff::serverDisconnect() 
{
    qCDebug(ATCORE_CLIENT) << (tr("Server disconnected"));
    m_socket->deleteLater();
    exit(0);
}

void ClientStuff::sendCommand(const QString &comm) 
{
    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint16(0) << comm;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    m_socket->write(arrBlock);
}

void ClientStuff::closeConnection() 
{
    disconnect(m_socket, &QSslSocket::connected, 0, 0);

    switch (m_socket->state()) {
    case 0:
        m_socket->disconnectFromHost();
        break;
    case 2:
        m_socket->abort();
        break;
    default:
        m_socket->abort();
   }
}
